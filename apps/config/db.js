// const mysql = require('mysql');
// // initialize an instance of Sequelize
// //create database connection
// var conn = mysql.createConnection({
//   // host     : 'localhost',
//   // user     : 'root',
//   // password : '',
//   // database : 'softcolpryst'

//   host     : 'appsontechnologies.in',
//   port     : '3306',
//   user     : 'softcolpryst_usr',
//   password : 'ctUyJlFmNfZ3',
//   database : 'softcolpryst'  

// });
// conn.connect(function(err){

// if(!err) {
//     console.log("Database is connected ... ");    
// } else {
//     console.log("Error connecting database ... ",err);    
// }
// });


//  module.exports = conn;

const mysql = require('mysql');

var db_config = {
  host     : 'appsontechnologies.in',
  port     : '3306',
  user     : 'softcolpryst_usr',
  password : 'ctUyJlFmNfZ3',
  database : 'softcolpryst' 
};

var conn;

function handleDisconnect() {
  conn = mysql.createConnection(db_config); // Recreate the connection, since
                                                  // the old one cannot be reused.

  conn.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err);
      setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
    }                                     // to avoid a hot loop, and to allow our node script to
  });                                     // process asynchronous requests in the meantime.
                                          // If you're also serving http, display a 503 error.
  conn.on('error', function(err) {
    console.log('db error', err);
    if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
      handleDisconnect();                         // lost due to either server restart, or a
    } else {                                      // connnection idle timeout (the wait_timeout
      throw err;                                  // server variable configures this)
    }
  });
}

handleDisconnect();

module.exports = conn;