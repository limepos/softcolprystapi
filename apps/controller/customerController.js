var conn = require('../config/db');
var config = require('../config/emailConfig');
const bcrypt = require("bcrypt");
module.exports.createCustomer = function (req, res) {

  var customers = {
    "business": req.body.business_name,
    "company": req.body.company_name,
    "email": req.body.mail,
    "mobile": req.body.phone,
    "telephone": req.body.landline,
    "department": req.body.department,
    "city": req.body.city,
    "nit": req.body.card_nit,
    "legalRep": req.body.legal_rep,
    "employeesdirect": req.body.direct_employees,
    "employeesindirect": req.body.indirect_direct_employess,
    "area": req.body.area,
    "subcompanies": req.body.activate_account_links,
    "costcenter": req.body.cost_center,
    "workcenter": req.body.work_center,
    "billingcoutoffdate": req.body.billing_date,
    "totalemployee": req.body.no_of_employee,
    "photos": req.body.attach_photo,
    "address": req.body.address
  }


  // console.log("productdata", customers);

  conn.query('INSERT INTO customers SET ?', customers, function (error, results, fields) {
    if (error) {
       console.log("error",error);
    } else { 
          let data =  req.body.items;
          let id = results.insertId;
          let i;
          console.log(data.length);
           for(i =0; i < data.length; i++ ){

            var customerServices = {
              "customer_id" : id,
              "service_id": data[i].service,
              "charges" : data[i].price
            }
            console.log("customerServices", customerServices);
            
             conn.query('INSERT INTO customer_service_charges SET ?', customerServices, function (error, results, fields){
 
             });

           }

           let encryptedString = bcrypt.hashSync('password', 10);
         var users={
        "firstName":req.body.company_name,
        "lastName":req.body.company_name,
        "email": req.body.mail,
        "mobile":req.body.phone,
        "department" : req.body.department,
        "city" : req.body.city,
        "usertype" : "13",  //Customer type Number
        "identity" : "-",
        "possition" :"-",
        "area" :"-",
        "profession" :  "-",
        "photo" :  "-",
        "password" : encryptedString
    }
        conn.query('INSERT INTO users SET ?',users, function (error, results, fields) {
          if (error) {
            console.log("error",error);
          }else {
            res.json({
              status: true,
              data: results,
              message: 'customer add successful.'
            })
          }
        })
      
    }
  });
}

module.exports.getAllCustomers = function (req, res) {
  conn.query('SELECT * from customers', function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}


module.exports.customerFilter = function (req, res) {

  let data = req.body;
   let nit_cedula = data.nit_cedula;
   let business_name = '%' + data.business_name + '%';
   let companyname = '%' + data.companyname + '%';
   let email = data.email;

  conn.query('SELECT * from customers where customer_status = 0 AND ( business LIKE ? OR business LIKE ? ) AND  email = ? AND nit = ? ',[business_name, companyname, email, nit_cedula ],function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}



module.exports.addNewEmployee = function (req, res) {

  var employee = {
    "customer_id": req.body.customer_id,
    "firstName": req.body.name,
    "lastName": req.body.last_name,
    "email": req.body.mail,
    "phone": req.body.phone,
    "mobile": req.body.mobile,
    "city": req.body.city,
    "department": req.body.department,
    "cartnumber": req.body.no_card,
    "position": req.body.position,
    "area": req.body.area,
    "profession": req.body.profession,
    "administrator": req.body.administrator,
    "observer": req.body.observer,
    "address": req.body.address
  }


   console.log("productdata", employee);

  conn.query('INSERT INTO customer_employees SET ?', employee, function (error, results, fields) {
    if (error) {
       console.log("error",error);
    } else {
        res.json({
            status: true,
            data: results,
            message: 'customer add successful.'
          })
    }
  });
}


module.exports.getAllCustomersEmployee = function (req, res) {
   let id = req.params.id;
  conn.query('SELECT * from customer_employees where customer_id = ?',[id] ,function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

module.exports.getAllCustomersEmployee = function (req, res) {
  let id = req.params.id;
 conn.query('SELECT * from customer_employees where customer_id = ?',[id] ,function (error, results, fields) {
   if (error) throw error;
   res.send({"status": 200, "data": results});
 });

}


module.exports.employeeFilter = function (req, res) {
  let id = req.params.id;

  let data = req.body;

  console.log(data);

  let id_card = data.id_card;
  let name = '%' + data.name + '%';
  let last_name = '%' + data.last_name + '%';

 conn.query('SELECT * from customer_employees where customer_id = ? AND ( firstName LIKE ? OR lastName LIKE ? OR cartnumber = ? )',[id, name, last_name, id_card] ,function (error, results, fields) {
   if (error) throw error;
   res.send({"status": 200, "data": results});
 });

}






