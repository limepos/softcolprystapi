const express = require('express'),
  path = require('path'),
  cors = require('cors'),
  multer = require('multer'),
  bodyParser = require('body-parser');

  const PATH = '../uploads';



exports.upload = async => {
    let storage = multer.diskStorage({
        destination: (req, file, cb) => {
          cb(null, PATH);
        },
        filename: (req, file, cb) => {
          cb(null, file.fieldname + '-' + Date.now())
        }
      });
      
      let upload = multer({
        storage: storage
      });
    return upload;
  };
