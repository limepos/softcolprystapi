var conn = require('../config/db');
const bcrypt = require("bcrypt");
var config = require('../config/emailConfig');
const nodemailer = require("nodemailer");
var express=require("express");
const jwt = require("jsonwebtoken");
const passportJWT = require("passport-jwt");
let ExtractJwt = passportJWT.ExtractJwt;
let JwtStrategy = passportJWT.Strategy;
let jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = "appsonsdevlopertsecrerkey@123";

module.exports.home=function(req,res){

  res.json({
    status:true,
    message:'api is running.'
})
}

module.exports.newUser=function(req, res){

  let encryptedString = bcrypt.hashSync('password', 10);
    var users={
        "firstName":req.body.name,
        "lastName":req.body.last_name,
        "email": req.body.email,
        "mobile":req.body.mobile,
        "department" : req.body.department,
        "city" : req.body.city,
        "usertype" : req.body.user_type,
        "identity" : req.body.id_card,
        "possition" :req.body.position,
        "area" :req.body.area,
        "profession" :  req.body.profession,
        "photo" :  req.body.attach_photo,
        "password" : encryptedString
    }
    conn.query('INSERT INTO users SET ?',users, function (error, results, fields) {
      if (error) {
        res.json({
            status:false,
            message:'there are some error with query',
            data : error
        })
      }else{
          res.json({
            status:true,
            data:results,
            message:'user registered successfully'
        })
      }
    });
}


module.exports.getUsers = function (req, res) {
  conn.query('SELECT users_types.typeName as usertypetitle, users.* from users LEFT JOIN users_types ON users_types.type_id = users.usertype where users.staff_status = 1 AND users.usertype  < 13 ', function (error, results, fields) {
		if (error) throw error;
	  	res.send({"status": 200, "data": results});
	});

}

module.exports.getUserById =  function (user_id) {
 conn.query('SELECT * from users where user_id = ?',[user_id], function (error, results, fields) {
  });
  

}

module.exports.getUserByEmail = function (email) {
  console.log("email => ", email);
  conn.query('SELECT * from users where email = ?',[email], function (error, results, fields) {
    if (error) throw error;
    
	});
}

module.exports.userLogin = function (req, res) {

  //console.log("-->", req.body)
 
  const { email, password } = req.body;
  if (email && password) {

    //console.log(conn);
   
      conn.query('SELECT * from users where email = ?',[email], function (error, results, fields) {
      
        console.log("--> Data getting.",results);
        if (results.length == 0) {

          console.log("--> Zero Data available");

          res.status(401).json({
            message: "No such user found",
            status: false
          });
        }
        else {
          console.log("--> Data available");
    //  let loginuser = JSON.stringify(results);
        let countshop = JSON.stringify(results);
        let loginusers = JSON.parse(countshop);
        let loginuser = loginusers[0];

      
      console.log("loginuser", loginuser);
     
      if ( bcrypt.compareSync(password, loginuser.password) && loginuser.staff_status == 1 ) {
      
        console.log("--> Password matching..");
        let payload = {
          user_id: loginuser.user_id
        };

         if(loginuser.usertype == 13 ){

          console.log("--> Login user is type 13");

          conn.query('SELECT * from customers where email = ?',[email] , function (error, results, fields) {
            
            let cc = JSON.stringify(results);
            let cust = JSON.parse(cc);
            let cuss = cust[0];
            loginuser.customer_id = cuss.customer_id;

              let token = jwt.sign(payload, jwtOptions.secretOrKey);
              res.status(200).json({
                message: "ok",
                token: token,
                data: loginuser,
                status: true
              });
            })
         }else {
           
          let token = jwt.sign(payload, jwtOptions.secretOrKey);
          res.status(200).json({
            message: "ok",
            token: token,
            data: loginuser,
            status: true
          });
         }
  
      } else {
        console.log("--> Password are not matching...");
        res.status(401).json({
          message: "Password is incorrect",
          status: false
        });
      }
    }
    }); 
  }else{
    console.log("--> Data is not available");

    res.status(401).json({
      message: "Password is incorrect",
      status: false
    });
  }

}
