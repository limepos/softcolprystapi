var conn = require('../config/db');
var config = require('../config/emailConfig');
module.exports.createProduct = function (req, res) {
  var productdata = {
    "proudctName": req.body.product_name,
    "productCode": req.body.code
  }
   console.log("productdata", productdata);

  conn.query('INSERT INTO products SET ?', productdata, function (error, results, fields) {
    if (error) {
      res.json({
        status: false,
        message: 'there are some error with query',
      })
    } else { 
        res.json({
            status: true,
            data: results,
            message: 'Product add successful.'
          })
    }
  });
}

module.exports.getProduct = function (req, res) {
  conn.query('SELECT * from products', function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

