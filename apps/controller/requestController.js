var conn = require('../config/db');
var config = require('../config/emailConfig');
module.exports.createRequest = function (req, res) {

  var requestdata = {
    "customer_id": req.body.company,
    "businessUser": req.body.user,
    "product_id": req.body.area,
    "service_id": req.body.type_of_service,
    "identity_document": req.body.id_doc,
    "typeofDocument": req.body.document_type,
    "ducumentNumber": req.body.documentnumber,
    "firstname": req.body.candidate_name,
    "lastname": req.body.candidate_last_name,
    "email": req.body.email,
    "direction": req.body.direction,
    "phone": req.body.phone,
    "mobile": req.body.cellphone,
    "departmant": req.body.department,
    "city": req.body.city,
    "neighborhood": req.body.neighborhood,
    "position": req.body.position,
    "expenditureCenter": req.body.expense_center,
    "observations": req.body.observations,
    "description": req.body.description,
    "details": req.body.details,
    "attachphoto": req.body.attach_photo,
    "attachdocuments": req.body.attachdoc,
    "socialresion": req.body.socialresone,
    "typeofservice": req.body.servicetype,
    "testType": req.body.testtype
  }

  conn.query('SELECT * from users where usertype = "2" ORDER BY nofcurrent_req ASC LIMIT 1', function (error, results, fields) {

       if(error){
         console.log(error);
       }
    
        let users = JSON.stringify(results);
      // console.log(users);
        let userdata = JSON.parse(users);
       // console.log("userdata", userdata);
        if(userdata.length > 0 ){
          // console.log("userdata", userdata);
           let user = userdata[0];
            requestdata.assignTo = user.user_id;
            requestdata.assignDate = new Date();

           let increment = user.nofcurrent_req + 1;
          
         //  console.log("user", user)
         //  console.log("increment", increment)
           conn.query('UPDATE users SET nofcurrent_req = '+increment+' where user_id = ? ', [user.user_id], function (error, results, fields) {
            if (error) {
               console.log("error", error)
            }
           })
         }
        
    console.log(requestdata);
      
  conn.query('INSERT INTO requests SET ?', requestdata, function (error, results, fields) {
    if (error) {
      res.json({
        status: false,
        message: 'there are some error with query',
      })
    } else {
      var requestdataStatus = {
        request_id:results.insertId
      }

      console.log(results.insertId);
      
      conn.query('INSERT INTO request_status SET ?', [requestdataStatus], function (error, results, fields) {
        
        

      })


      // var myDate = new Date(new Date().getTime() + (2 * 24 * 60 * 60 * 1000));
      // var today = new Date();
      // var dd = String(today.getDate()).padStart(2, '0');
      // var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      // var yyyy = today.getFullYear();
      // today = mm + '/' + dd + '/' + yyyy;
      // var twotoday = new Date(new Date().getTime() + (2 * 24 * 60 * 60 * 1000));;
      // var ddd = String(twotoday.getDate()).padStart(2, '0');
      // var mmm = String(twotoday.getMonth() + 1).padStart(2, '0'); //January is 0!
      // var yyyyy = twotoday.getFullYear();
      // twotoday = mmm + '/' + ddd + '/' + yyyyy;
      
      // const transporter = config.transporter();
      // const mailOptions = {
      //   from: config.fromEmail(),
      //   to: users.email,
      //   subject: "Nueva solicitud",
      //   html:
      //     "<p>Apreciado.</p><p> " + users.businessUser + " </p> <p> usted ha iniciado su proceso de estudio de seguridad, solicitado por la empresa " + users.firstname + " " + users.lastname + ", para tal efecto lo estará contactando uno de nuestros funcionarios para la ejecución del mismos. </p> <p>  Entre tanto usted debe ingresar a nuestra plataforma para adjuntar su información y soportes documentales.</p> <p>por favor ingrese a www.colpryst........   Asi mismo le solicitamos este proceso sea realizado entre hoy. " + today + " y " + twotoday + "."
      // };
      // transporter.sendMail(mailOptions, function (error, info) {
      //   if (error) {
      //     res.json({
      //       status: false,
      //       data: error,
      //       message: 'email is not send'
      //     })
      //   } else {
      //     res.json({
      //       status: true,
      //       data: results,
      //       message: 'Email is send success full'
      //     })
      //   }
      res.json({
        status: true,
        data: results,
        message: 'Email is send success full'
        }) 
        }
      });
      })
    }




module.exports.getRequest = function (req, res) {
   
  // conn.query('SELECT * from requests', function (error, results, fields) {
	// 	if (error) throw error;
	// 	res.send({"status": 200, "data": results});
  // });
  
  conn.query('SELECT customers.business as cbusiness, services.serviceName as services, products.proudctName as servicetype, users.firstName as assignTofname, users.lastName as assignTolname, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id LEFT JOIN products ON products.product_id = requests.product_id LEFT JOIN users ON users.user_id = requests.assignTo where requests.requestStatus = 1', function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}
module.exports.updateRequestById = function (req, res) {

let data = req.body;
let request_id  =  req.params.id;
  
  conn.query('UPDATE requests SET ? where request_id = ? ', [data,  request_id], function (error, results, fields) {
    if (error) throw error;
		res.send({"status": 200, "data": results});
   })

}

module.exports.getRequestByCustomerId = function (req, res) {
  let customer_id  =  req.params.id;
  console.log("-->", customer_id);
  conn.query('SELECT customers.business as cbusiness, services.serviceName as services, products.proudctName as servicetype, users.firstName as assignTofname, users.lastName as assignTolname, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id LEFT JOIN products ON products.product_id = requests.product_id LEFT JOIN users ON users.user_id = requests.assignTo where requests.requestStatus = 1 AND requests.customer_id = ? ',[customer_id], function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

module.exports.getRequestById = function (req, res) {
  let request_Id  =  req.params.id;
 // console.log("-->", customer_id);
  conn.query('SELECT customers.business as cbusiness, services.serviceName as services, products.proudctName as servicetype, users.firstName as assignTofname, users.lastName as assignTolname, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id LEFT JOIN products ON products.product_id = requests.product_id LEFT JOIN users ON users.user_id = requests.assignTo where requests.request_Id = ? AND  requests.requestStatus = 1',[request_Id], function (error, results, fields) {
		if (error) throw error;
		res.send({"status 1": 200, "data": results});
	});

}

module.exports.bulkCreateRequest = function (req, res) {
   //  console.log("request", req.body.d)
    var obj = JSON.parse(req.body.d);
  //  console.log("obj", obj.Sheet1);
  let length =  Object.keys(obj).length;
  var requestdata;
   for (let index = 0; index < length; index++) {
     //const element = obj.Sheet1[index].company;

      requestdata = {
      "customer_id": obj.Sheet1[index].company,
      "businessUser": obj.Sheet1[index].user,
      "product_id": obj.Sheet1[index].area,
      "service_id": obj.Sheet1[index].type_of_service,
      "identity_document": obj.Sheet1[index].id_doc,
      "typeofDocument": obj.Sheet1[index].document_type,
      "ducumentNumber": obj.Sheet1[index].documentnumber,
      "firstname": obj.Sheet1[index].candidate_name,
      "lastname": obj.Sheet1[index].candidate_last_name,
      "email": obj.Sheet1[index].email,
      "direction":obj.Sheet1[index].direction,
      "phone": obj.Sheet1[index].phone,
      "mobile": obj.Sheet1[index].cellphone,
      "departmant": obj.Sheet1[index].department,
      "city": obj.Sheet1[index].city,
      "neighborhood": obj.Sheet1[index].neighborhood,
      "position": obj.Sheet1[index].position,
      "expenditureCenter": obj.Sheet1[index].expense_center,
      "observations": obj.Sheet1[index].observations,
      "description": obj.Sheet1[index].description,
      "details": obj.Sheet1[index].details,
      "attachphoto": "attachment",
      "attachdocuments": "attachment",
      "socialresion": obj.Sheet1[index].socialresone,
      "typeofservice": obj.Sheet1[index].servicetype,
      "testType": obj.Sheet1[index].testtype
    }

      
    console.log(requestdata);
    
      conn.query('SELECT * from products where proudctName = ?',[requestdata.product_id], function (error, results, fields) {        
        let productid = JSON.stringify(results);
        let loginusers = JSON.parse(productid);
       // console.log("loginuser", loginusers);
       
      //  console.log("loginuser", loginuser.product_id);
         if(loginusers.length > 0 ){
           let loginuser = loginusers[0];
           requestdata.product_id = loginuser.product_id;
         }
       var userEmail = requestdata.businessUser
       conn.query('SELECT * from customer_employees where email = ?',[userEmail] ,function (error, results, fields) {
        
        let usereid = JSON.stringify(results);
        let usereids = JSON.parse(usereid);
        
    //   console.log("loginuser", usereids);
        if(usereids.length > 0 ){
          let employee = usereids[0];
          requestdata.businessUser = employee.employe_id;
         }
      
       var customerEmail =  requestdata.customer_id
         console.log()
        conn.query('SELECT * from customers where email = ? ',[customerEmail], function (error, results, fields) {
           
          let userid = JSON.stringify(results);
          let userids = JSON.parse(userid);
          
         
         
          if(userids.length > 0 ){
            let user = userids[0];
            console.log("loginuser", user);
            requestdata.customer_id = user.customer_id;
           }

          conn.query('SELECT * from services where serviceName = ? ', [requestdata.service_id], function (error, results, fields) {
              
            let servesid = JSON.stringify(results);
            let sids = JSON.parse(servesid);
           
          //  console.log("loginuser", loginuser.product_id);
            if(sids.length > 0 ){
              let serrvi = sids[0];
              requestdata.service_id = serrvi.service_Id;
             }
           
             console.log("loginuser.product_id",requestdata);

             conn.query('SELECT * from users where usertype = "2" ORDER BY nofcurrent_req ASC LIMIT 1', function (error, results, fields) {

              if(error){
                console.log(error);
              }
           
               let users = JSON.stringify(results);
             // console.log(users);
               let userdata = JSON.parse(users);
              // console.log("userdata", userdata);
               if(userdata.length > 0 ){
                 // console.log("userdata", userdata);
                  let user = userdata[0];
                   requestdata.assignTo = user.user_id;
                   requestdata.assignDate = new Date();
       
                  let increment = user.nofcurrent_req + 1;
                 
                  console.log("user", user)
                  console.log("increment", increment)
                  conn.query('UPDATE users SET nofcurrent_req = '+increment+' where user_id = ? ', [user.user_id], function (error, results, fields) {
                   if (error) {
                      console.log("error", error)
                   }
                  })
                }

              conn.query('INSERT INTO requests SET ?', requestdata, function (error, results, fields) {
              });
            });

        });
      });
    });     
     
     })
   }
    res.json({
      status: true,
      message: 'Email is send success full'
    })
}


module.exports.bulkCreateRequestByClient = function (req, res) {
    console.log("request", req.params.id)
   var obj = JSON.parse(req.body.d);
   var customerID = req.params.id;
 let length =  Object.keys(obj).length;
 var requestdata;
  for (let index = 0; index < length; index++) {
    //const element = obj.Sheet1[index].company;

     requestdata = {
     "customer_id": customerID,
     "businessUser": obj.Sheet1[index].user,
     "product_id": obj.Sheet1[index].area,
     "service_id": obj.Sheet1[index].type_of_service,
     "identity_document": obj.Sheet1[index].id_doc,
     "typeofDocument": obj.Sheet1[index].document_type,
     "ducumentNumber": obj.Sheet1[index].documentnumber,
     "firstname": obj.Sheet1[index].candidate_name,
     "lastname": obj.Sheet1[index].candidate_last_name,
     "email": obj.Sheet1[index].email,
     "direction":obj.Sheet1[index].direction,
     "phone": obj.Sheet1[index].phone,
     "mobile": obj.Sheet1[index].cellphone,
     "departmant": obj.Sheet1[index].department,
     "city": obj.Sheet1[index].city,
     "neighborhood": obj.Sheet1[index].neighborhood,
     "position": obj.Sheet1[index].position,
     "expenditureCenter": obj.Sheet1[index].expense_center,
     "observations": obj.Sheet1[index].observations,
     "description": obj.Sheet1[index].description,
     "details": obj.Sheet1[index].details,
     "attachphoto": "attachment",
     "attachdocuments": "attachment",
     "socialresion": obj.Sheet1[index].socialresone,
     "typeofservice": obj.Sheet1[index].servicetype,
     "testType": obj.Sheet1[index].testtype
   }

     
   console.log(requestdata);
   
     conn.query('SELECT * from products where proudctName = ?',[requestdata.product_id], function (error, results, fields) {        
       let productid = JSON.stringify(results);
       let loginusers = JSON.parse(productid);
      // console.log("loginuser", loginusers);
      
     //  console.log("loginuser", loginuser.product_id);
        if(loginusers.length > 0 ){
          let loginuser = loginusers[0];
          requestdata.product_id = loginuser.product_id;
        }
      var userEmail = requestdata.businessUser
      conn.query('SELECT * from customer_employees where email = ?',[userEmail] ,function (error, results, fields) {
       
       let usereid = JSON.stringify(results);
       let usereids = JSON.parse(usereid);
       
   //   console.log("loginuser", usereids);
       if(usereids.length > 0 ){
         let employee = usereids[0];
         requestdata.businessUser = employee.employe_id;
        }
     
      // var customerEmail =  requestdata.customer_id
      //   console.log()
      //  conn.query('SELECT * from customers where email = ? ',[customerEmail], function (error, results, fields) {
          
      //    let userid = JSON.stringify(results);
      //    let userids = JSON.parse(userid);
         
        
        
      //    if(userids.length > 0 ){
      //      let user = userids[0];
      //      console.log("loginuser", user);
      //      requestdata.customer_id = customerID;
      //     }

         conn.query('SELECT * from services where serviceName = ? ', [requestdata.service_id], function (error, results, fields) {
             
           let servesid = JSON.stringify(results);
           let sids = JSON.parse(servesid);
          
         //  console.log("loginuser", loginuser.product_id);
           if(sids.length > 0 ){
             let serrvi = sids[0];
             requestdata.service_id = serrvi.service_Id;
            }
          
            console.log("loginuser.product_id",requestdata);

            conn.query('SELECT * from users where usertype = "2" ORDER BY nofcurrent_req ASC LIMIT 1', function (error, results, fields) {

             if(error){
               console.log(error);
             }
          
              let users = JSON.stringify(results);
            // console.log(users);
              let userdata = JSON.parse(users);
             // console.log("userdata", userdata);
              if(userdata.length > 0 ){
                // console.log("userdata", userdata);
                 let user = userdata[0];
                  requestdata.assignTo = user.user_id;
                  requestdata.assignDate = new Date();
      
                 let increment = user.nofcurrent_req + 1;
                
                 console.log("user", user)
                 console.log("increment", increment)
                 conn.query('UPDATE users SET nofcurrent_req = '+increment+' where user_id = ? ', [user.user_id], function (error, results, fields) {
                  if (error) {
                     console.log("error", error)
                  }
                 })
               }

             conn.query('INSERT INTO requests SET ?', requestdata, function (error, results, fields) {
             });
           });

       
     });
   });     
    
    })
  }
   res.json({
     status: true,
     message: 'Email is send success full'
   })
}


module.exports.getRequestByAnalystId = function (req, res) {
  let analyst_id  =  req.params.id;
//  console.log("-->", customer_id);
  conn.query('SELECT customers.business as cbusiness, services.serviceName as services, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id  where requests.assignTo = ?',[analyst_id], function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

module.exports.getRequestStatusById = function (req, res) {
  let request_id  =  req.params.id;
  conn.query('SELECT * from request_status where request_id = ?',[request_id], function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

module.exports.updateRequestStatusById = function (req, res) {
  let request_id  =  req.params.id;

   console.log(req.body);

  let data = req.body
  
  let joined,assignoadvisor,academicv,labourv,background,concept,preliminary,finialized,physical,approval,joindate='',assignadvisordate ='',finalizedate ='' ;
  if(data.joined){
    joined = "true"   
  }else{
    joined = "false"   
  }

  if(data.assignoadvisor){
    assignoadvisor = "true"   
  }else{
    assignoadvisor = "false"   
  }

  if(data.academicv){
    academicv = "true"   
  }else{
    academicv = "false"   
  }
  
  if(data.labourv){
    labourv = "true"   
  }else{
    labourv = "false"   
  }
 
  if(data.preliminary){
    preliminary = "true"   
  }else{
    preliminary = "false"   
  }

  if(data.finialized){
    finialized = "true"   
  }else{
    finialized = "false"   
  }

  if(data.physical){
    physical = "true"   
  }else{
    physical = "false"   
  }
 
  if(data.approval){
    approval = "true"   
  }else{
    approval = "false"   
  }

  background = data.background
  concept = data.concept
  data.request_id

  if(joined){
    joindate = new Date();
  }
  if(assignoadvisor){
    assignadvisordate = new Date();
  }
  if(finialized){
    finalizedate = new Date();
  }

  conn.query('UPDATE request_status SET joined = ?, assignoadvisor = ?,academicv = ?,labourv = ?,background = ?,concept = ?,preliminary = ?,finialized = ?,physical = ?,approval = ?,joindate = ?,assignadvisordate = ?, finalizedate = ? where request_id = ? ', [joined,assignoadvisor,academicv,labourv,background,concept,preliminary,finialized,physical,approval, joindate,assignadvisordate,finalizedate,  request_id], function (error, results, fields) {
    if (error) {
       console.log("error", error)
    }
   })
}


module.exports.deleteById = function (req, res) {
  let request_Id  =  req.params.id;
 // console.log("-->", customer_id);
  conn.query('UPDATE requests SET requestStatus = 0  where request_Id = ?', [request_Id], function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": 'ok'});
	});

}

module.exports.getfilterRequest = function (req, res) {

   let data = req.body;

  

   console.log(data);

   let type_of_service = data.type_of_service;

   let name = '%' + data.name + '%';

   let id_card = data.id_card;

   let assigned_to = data.assigned_to;

  conn.query('SELECT customers.business as cbusiness, services.serviceName as services, products.proudctName as servicetype, users.firstName as assignTofname, users.lastName as assignTolname, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id LEFT JOIN products ON products.product_id = requests.product_id LEFT JOIN users ON users.user_id = requests.assignTo where requests.requestStatus = 1 AND ( requests.firstname LIKE N? OR requests.identity_document = ? ) AND requests.service_id = ? AND requests.assignTo = ? ', [ name, id_card, type_of_service, assigned_to ],function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

module.exports.getfilterRequestanalyst = function (req, res) {

  let data = req.body;
  
  let analystID  =  req.params.id;

  console.log(data);

  let type_of_service = data.type_of_service;

  let name = '%' + data.name + '%';

  let id_card = data.id_card;

 conn.query('SELECT customers.business as cbusiness, services.serviceName as services, products.proudctName as servicetype, users.firstName as assignTofname, users.lastName as assignTolname, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id LEFT JOIN products ON products.product_id = requests.product_id LEFT JOIN users ON users.user_id = requests.assignTo where requests.requestStatus = 1 AND ( requests.firstname LIKE N? OR requests.identity_document = ? ) AND requests.service_id = ? AND requests.assignTo = ? ', [ name, id_card, type_of_service, analystID ],function (error, results, fields) {
   if (error) throw error;
   res.send({"status": 200, "data": results});
 });

}

module.exports.getfilterRequestclient = function (req, res) {

  let data = req.body;
  
  let clientID  =  req.params.id;

  console.log(data);

  let type_of_service = data.type_of_service;

  let name = '%' + data.name + '%';

  let id_card = data.id_card;

 conn.query('SELECT customers.business as cbusiness, services.serviceName as services, products.proudctName as servicetype, users.firstName as assignTofname, users.lastName as assignTolname, requests.* from requests LEFT JOIN customers ON customers.customer_id = requests.customer_id LEFT JOIN services ON services.service_Id = requests.service_id LEFT JOIN products ON products.product_id = requests.product_id LEFT JOIN users ON users.user_id = requests.assignTo where requests.requestStatus = 1 AND ( requests.firstname LIKE N? OR requests.identity_document = ? ) AND requests.service_id = ? AND requests.customer_id = ? ', [ name, id_card, type_of_service, clientID ],function (error, results, fields) {
   if (error) throw error;
   res.send({"status": 200, "data": results});
 });

}

module.exports.getAnalystst = function (req, res) {
conn.query('SELECT * from users where usertype = "2" ', function (error, results, fields) {

  if(error){
    console.log(error);
  }
  res.send({"status": 200, "data": results});
})

}


