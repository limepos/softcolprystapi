var conn = require('../config/db');
var config = require('../config/emailConfig');
module.exports.createService = function (req, res) {

  var productdata = {
    "serviceName": req.body.service_name,
    "serviceCode": req.body.code,
    "product_id": req.body.associated_product
  }

  console.log("productdata", productdata)

  conn.query('INSERT INTO services SET ?', productdata, function (error, results, fields) {
    if (error) {
      res.json({
        status: false,
        message: 'there are some error with query',
      })
    } else { 
        res.json({
            status: true,
            data: results,
            message: 'Service add successful.'
          })
    }
  });
}

module.exports.getService = function (req, res) {
  conn.query('SELECT * from services', function (error, results, fields) {
		if (error) throw error;
		res.send({"status": 200, "data": results});
	});

}

module.exports.getAllService = function (req, res) {

    conn.query('SELECT * from services JOIN products ON services.product_id = products.product_id', function (error, results, fields) {
          if (error) throw error;
          res.send({"status": 200, "data": results});
      });
  
  }

  module.exports.getFilterProduct = function (req, res) {

   let data = req.body;
 
    let product_code = data.product_code;
    let product_name = data.product_name;


    conn.query('SELECT * from services JOIN products ON services.product_id = products.product_id where products.proudctName = ? OR products.productCode = ?',[product_name, product_code ],function (error, results, fields) {
          if (error) throw error;
          res.send({"status": 200, "data": results});
      });
  
  }

  module.exports.getServiceByProduct = function (req, res) {

    let product_id  =  req.params.id;
    conn.query('SELECT * from services where product_id = ? ', [product_id], function (error, results, fields) {
      if (error) throw error;
      res.send({"status": 200, "data": results});
    });
  
  }

   
