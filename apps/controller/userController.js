var conn = require('../config/db');

module.exports.userRollers = function (req, res) {
    conn.query('SELECT * from users_types', function (error, results, fields) {
          if (error) throw error;
          res.send({"status": 200, "data": results});
      });
  
  }

  module.exports.filterUser = function (req, res) {
    console.log(req.body);
  
    let data = req.body;
    
   let id_card = data.id_card;

   let name = '%' + data.name + '%';
   let last_name = '%' + data.last_name + '%';

   let user_type = data.user_type;

    conn.query('SELECT users_types.typeName as usertypetitle, users.* from users LEFT JOIN users_types ON users_types.type_id = users.usertype where users.staff_status = 1 AND users.usertype  < 13 AND (users.firstName LIKE ? OR users.lastName LIKE ? OR users.identity = ? OR users.usertype = ?) ',[name, last_name, id_card, user_type ],function (error, results, fields) {
          if (error) throw error;
            res.send({"status": 200, "data": results});
      });
  } 

  module.exports.getUserById = function (req, res) {

    let userId  =  req.params.id;
    conn.query('SELECT users_types.typeName as usertypetitle, users.* from users LEFT JOIN users_types ON users_types.type_id = users.usertype where users.user_id = ?',[userId],function (error, results, fields) {
          if (error) throw error;
          res.send({"status": 200, "data": results});
      });
  
  }

  module.exports.updateUserById = function (req, res) {

   console.log(req.body);

   var users={
    "firstName":req.body.name,
    "lastName":req.body.last_name,
    "email": req.body.email,
    "mobile":req.body.mobile,
    "department" : req.body.department,
    "city" : req.body.city,
    "usertype" : req.body.user_type,
    "identity" : req.body.id_card,
    "possition" :req.body.position,
    "profession" :  req.body.profession
}

    let userId  =  req.params.id;
    conn.query('UPDATE users SET ? where user_id = ?',[users, userId],function (error, results, fields) {
          if (error) throw error;
          res.send({"status": 200, "data": results});
      });
  
  }

  module.exports.deleteUserById = function (req, res) {
    let userId  =  req.params.id;
    conn.query('UPDATE users SET staff_status = 0 where user_id = ?',[userId],function (error, results, fields) {
      if (error) throw error;
      res.send({"status": 200, "data": results});
  });
  }
  
  