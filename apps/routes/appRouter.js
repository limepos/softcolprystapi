module.exports = function (app) {
    require('./loginRouter')(app);
    require('./userRouter')(app);
    require('./requestRouter')(app);
    require('./productRouter')(app);
    require('./serviceRouter')(app);
    require('./customerRouter')(app);
    
}
