module.exports = function (app) {
    var customer = require('../controller/customerController');
    const passport = require('passport');
    
    app.post('/api/new-customer',passport.authenticate("jwt", {
        session: false
      }), customer.createCustomer);

    app.get('/api/customers',passport.authenticate("jwt", {
      session: false
    }), customer.getAllCustomers);

    app.post('/api/add-employee',passport.authenticate("jwt", {
      session: false
    }), customer.addNewEmployee);

    app.get('/api/get-employees/:id',passport.authenticate("jwt", {
      session: false
    }), customer.getAllCustomersEmployee);

    app.post('/api/customer/filter',passport.authenticate("jwt", {
      session: false
    }), customer.customerFilter);

    app.post('/api/employee/filter/:id',passport.authenticate("jwt", {
      session: false
    }), customer.employeeFilter);

    
  
  }