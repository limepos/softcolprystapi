module.exports = function (app) {
     var login = require('../controller/loginController');
     var conn = require('../config/db');
     const bcrypt = require("bcrypt");
     const jwt = require("jsonwebtoken");
     const passport = require("passport");
     const passportJWT = require("passport-jwt");
     var config = require("../config/appConfig");
     let ExtractJwt = passportJWT.ExtractJwt;
     let JwtStrategy = passportJWT.Strategy;
     let jwtOptions = {};
     jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
     jwtOptions.secretOrKey = "appsonsdevlopertsecrerkey@123";
     let strategy = new JwtStrategy(jwtOptions, function(jwt_payload, next) {
       console.log("payload received", jwt_payload);
       conn.query('SELECT * from users where user_id = ?',[jwt_payload.user_id], function (error, results, fields) {
        
        let countshop = JSON.stringify(results);
        let loginusers = JSON.parse(countshop);
        let loginuser = loginusers[0];
       if (loginuser) {
         next(null, loginuser);
       } else {
         next(null, false);
       }
      });

     });
   
     passport.use(strategy);
   
     app.use(passport.initialize());
     
     app.post('/api/new-user',passport.authenticate("jwt", {
      session: false
    }), login.newUser);

     app.get('/api/get-users', login.getUsers);
     app.get('/', login.home);
     app.post("/api/login", login.userLogin);
         
}



 