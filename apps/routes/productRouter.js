module.exports = function (app) {
    var request = require('../controller/productController');
    const passport = require('passport');
    
    app.post('/api/new-product',passport.authenticate("jwt", {
        session: false
      }), request.createProduct);

    app.get('/api/get-product',passport.authenticate("jwt", {
        session: false
      }), request.getProduct);

     
}



