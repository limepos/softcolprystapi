const multer = require('multer');
const path = require('path');

module.exports = function (app) {
    var request = require('../controller/requestController');
    const passport = require('passport');
     
    const DIR = './apps/uploads';
 
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, DIR);
    },
    filename: (req, file, cb) => {
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});
let upload = multer({storage: storage});

app.post('/api/upload/photo',upload.single('attach_photo'), function (req, res) {
  if (!req.file) {
     // console.log("Your request doesn’t have any file");
      return res.send({
        success: false
      });
  
    } else {

    // console.log('Your file has been received successfully', req);
      return res.send({
        success: true,
        data : req.file
      })
    }
});

app.post('/api/upload/doc',upload.single('attach_document'), function (req, res) {
  if (!req.file) {
      console.log("Your request doesn’t have any file");
      return res.send({
        success: false
      });
  
    } else {

      console.log('Your file has been received successfully', req);
      return res.send({
        success: true,
        data : req.file
      })
    }
});
    
    app.post('/api/new-request', passport.authenticate("jwt", {
        session: false
      }), request.createRequest);

    app.get('/api/get-request',passport.authenticate("jwt", {
        session: false
      }), request.getRequest);


    app.post('/api/bulk-new-request',passport.authenticate("jwt", {
       session: false
     }), request.bulkCreateRequest);

     app.post('/api/bulk-new-requests/client/:id',passport.authenticate("jwt", {
      session: false
    }), request.bulkCreateRequestByClient);

     app.get('/api/get-requests/:id',passport.authenticate("jwt", {
      session: false
    }), request.getRequestByCustomerId);

    app.get('/api/get-request/:id',passport.authenticate("jwt", {
      session: false
    }), request.getRequestById);


    app.get('/api/get-requests/analyst/:id',passport.authenticate("jwt", {
      session: false
    }), request.getRequestByAnalystId);

    app.get('/api/get-request_status/:id',passport.authenticate("jwt", {
      session: false
    }), request.getRequestStatusById);

    app.post('/api/update_request_status/:id',passport.authenticate("jwt", {
      session: false
    }), request.updateRequestStatusById);

    app.post('/api/update_request/:id',passport.authenticate("jwt", {
      session: false
    }), request.updateRequestById);

    app.get('/api/delete_request/:id',passport.authenticate("jwt", {
      session: false
    }), request.deleteById);

    app.post('/api/filter/request',passport.authenticate("jwt", {
      session: false
    }), request.getfilterRequest);

    app.post('/api/filter/request/analyst/:id',passport.authenticate("jwt", {
      session: false
    }), request.getfilterRequestanalyst);

    app.post('/api/filter/request/client/:id',passport.authenticate("jwt", {
      session: false
    }), request.getfilterRequestclient);

    app.get('/api/analysts',passport.authenticate("jwt", {
      session: false
    }), request.getAnalystst);

    

}



