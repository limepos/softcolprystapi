module.exports = function (app) {
    var request = require('../controller/serviceController');
    const passport = require('passport');
    
    app.post('/api/new-service',passport.authenticate("jwt", {
        session: false
      }), request.createService);

    app.get('/api/get-service',passport.authenticate("jwt", {
        session: false
      }), request.getService);


      app.get('/api/all-services',passport.authenticate("jwt", {
        session: false
      }), request.getAllService);


       app.get('/api/service/:id',passport.authenticate("jwt", {
        session: false
      }), request.getServiceByProduct);


      app.post('/api/products/filter',passport.authenticate("jwt", {
        session: false
      }), request.getFilterProduct);
}



