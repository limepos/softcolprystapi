module.exports = function (app) {
    
  var users = require('../controller/userController');
   const passport = require('passport');
   var conn = require('../config/db');
   
     app.get('/api/user-roles',passport.authenticate("jwt", {
       session: false
     }), users.userRollers);

     app.post('/api/users/filter',passport.authenticate("jwt", {
      session: false
    }), users.filterUser);

    app.get('/api/get-user/:id',passport.authenticate("jwt", {
      session: false
    }), users.getUserById);
     
    app.post('/api/user/update/:id',passport.authenticate("jwt", {
      session: false
    }), users.updateUserById);

    app.get('/api/delete/user/:id',passport.authenticate("jwt", {
      session: false
    }), users.deleteUserById);
    
 
}
