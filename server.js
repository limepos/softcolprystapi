const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;
const app = express();
app.use(express.static('../client'));
app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
   next();
  });

app.listen(port, function () {
    console.log('Express is running on port 3000');
});

var routes = require('./apps/routes/appRouter'); //importing route

routes(app); //register the route
